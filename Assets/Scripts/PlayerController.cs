﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	private float moveVelocity;
	public float jumpHeight;

	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	private bool grounded;

	private bool doubleJumped;

	private Animator anim;

	public Transform vacuumPoint;
	public Transform vacuumPivot;

	void Start () {
		anim = GetComponent<Animator> ();
	}

	void FixedUpdate() {
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
	}

	void Update () {

		if (grounded)
			doubleJumped = false;

		anim.SetBool ("Grounded", grounded);
		
		if (Input.GetKeyDown (KeyCode.Space) && grounded) 
		{
			Jump ();
		}

		if (Input.GetKeyDown (KeyCode.Space) && !doubleJumped && !grounded) 
		{
			Jump ();
			doubleJumped = true;
		}

		moveVelocity = 0f;

		if (Input.GetKey (KeyCode.D)) 
		{
			moveVelocity = moveSpeed;
		}

		if (Input.GetKey (KeyCode.A)) 
		{
			moveVelocity = -moveSpeed;
		}

		GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveVelocity, GetComponent<Rigidbody2D> ().velocity.y);

		anim.SetFloat ("Speed", Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x));

		if (GetComponent<Rigidbody2D> ().velocity.x > 0) 
		{
			transform.localScale = new Vector3 (1f, 1f, 1f);
		}
		else if (GetComponent<Rigidbody2D> ().velocity.x < 0) 
		{
			transform.localScale = new Vector3 (-1f, 1f, 1f);
			if (vacuumPivot.lossyScale.x < 0)
				vacuumPivot.localScale = new Vector3 (-1f, 1f, 1f);
		}

	}

	public void Jump() 
	{
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
	}		
}
