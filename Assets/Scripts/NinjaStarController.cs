﻿using UnityEngine;
using System.Collections;

public class NinjaStarController : MonoBehaviour {

	private bool stolen;

	public float speed;
	public float rotationSpeed;

	public GameObject enemyDeathParticle;
	public GameObject ninjaStarParticle;
	public GameObject hurtParticle;

	public int damage;

	// Use this for initialization
	void Start () {
		stolen = false;

		GetComponent<Rigidbody2D> ().velocity = new Vector2 (speed, GetComponent<Rigidbody2D> ().velocity.y);
		GetComponent<Rigidbody2D> ().angularVelocity = rotationSpeed;

		Destroy (gameObject, 10f);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonUp (1) && GetComponent<Transform> ().parent != null)
			GetComponent<Transform> ().SetParent (null);			
	}

	void OnTriggerEnter2D(Collider2D other)
	{

		if (other.tag == "Shield") {
			stolen = true;
			return;
		}

		if (other.tag == "Vacuum" && !other.GetComponent<Vacuum>().holdingProjectile) 
		{
			stolen = true;
			other.GetComponent<Vacuum> ().projectileSpeed = Mathf.Abs(speed);
			other.GetComponent<Collider2D> ().enabled = false;
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);
			GetComponent<Transform>().SetParent (other.GetComponent<Transform>());
			transform.position = other.transform.position;
			return;
		}

		if (other.tag == "Player") {
			HealthManager.HurtPlayer (damage);
			Instantiate (hurtParticle, other.transform.position, other.transform.rotation);
		} else if (other.tag == "Enemy" && stolen) {
			other.GetComponent<EnemyHealthManager> ().dealDamage (damage);
			Instantiate (hurtParticle, other.transform.position, other.transform.rotation);
		} else if (other.tag == "Enemy" && !stolen) {
			return;
		}
			
		Instantiate (ninjaStarParticle, transform.position, transform.rotation);
		if(GetComponent<Transform>().parent != null)
			GetComponentInParent<Vacuum> ().ReleaseVacuum ();
		Destroy (gameObject);
	}

}
