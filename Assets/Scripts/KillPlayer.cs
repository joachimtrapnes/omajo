﻿using UnityEngine;
using System.Collections;

public class KillPlayer : MonoBehaviour {

	private PlayerController player;

	public int pointsToRemove;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.name == "Player" && player.enabled == true) 
		{
			ScoreManager.AddPoints (-pointsToRemove);
			HealthManager.playerHealth = 0;
		}
	}
}