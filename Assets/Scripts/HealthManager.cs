﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour {

	public static int playerHealth;
	public int maxPlayerHealth;

	Text text;

	private LevelManager levelManager;
	private PlayerController player;

	// Use this for initialization
	void Start () {
	
		text = GetComponent<Text> ();

		playerHealth = maxPlayerHealth;

		levelManager = FindObjectOfType<LevelManager> ();
		player = FindObjectOfType<PlayerController> ();

	}
	
	// Update is called once per frame
	void Update () {
	
		if (playerHealth <= 0 && player.enabled == true) {
			levelManager.RespawnPlayer ();
		}

		text.text = "" + playerHealth;

	}

	public static void HurtPlayer(int damageDealt)
	{
		playerHealth -= damageDealt;
	}

	public void FullHealth()
	{
		playerHealth = maxPlayerHealth;
	}
}
