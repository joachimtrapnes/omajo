﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public GameObject currentCheckpoint;

	private PlayerController player;
	private CameraController camera;

	public GameObject deathParticle;
	public GameObject respawnParticle;
	public GameObject respawnPathParticle;

	public float respawnDelay;

	private Vector2 respawnVector;

	private HealthManager healthManager;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController>();
		healthManager = FindObjectOfType<HealthManager> ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void RespawnPlayer()
	{
		respawnVector = new Vector2 (currentCheckpoint.transform.position.x - player.transform.position.x, currentCheckpoint.transform.position.y - player.transform.position.y);

		StartCoroutine ("RespawnPlayerCo");
	}

	public IEnumerator RespawnPlayerCo()
	{
		Instantiate (deathParticle, player.transform.position, player.transform.rotation);
		Instantiate (respawnPathParticle, player.transform.position, player.transform.rotation);
		player.enabled = false;
		player.GetComponent<Rigidbody2D> ().isKinematic = true;
		player.GetComponent<Renderer>().enabled = false;
		player.GetComponent<Collider2D> ().enabled = false;
		if(FindObjectOfType<VaccumParticleController>() != null)
			FindObjectOfType<Vacuum> ().ReleaseVacuum ();

		player.GetComponent<Rigidbody2D> ().velocity = new Vector2 (respawnVector.x, respawnVector.y);
		yield return new WaitForSeconds (respawnDelay);
		player.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);
		player.transform.position = currentCheckpoint.transform.position;

		Debug.Log ("Player Respawn");
		healthManager.FullHealth ();

		player.GetComponent<Collider2D> ().enabled = true;
		player.GetComponent<Renderer>().enabled = true;
		player.GetComponent<Rigidbody2D> ().isKinematic = false;
		player.enabled = true;
		Instantiate (respawnParticle, currentCheckpoint.transform.position, currentCheckpoint.transform.rotation);
	}
}
