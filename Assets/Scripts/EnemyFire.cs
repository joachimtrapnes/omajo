﻿using UnityEngine;
using System.Collections;

public class EnemyFire : MonoBehaviour {

	public int pointsOnKill;

	public GameObject ninjaStar;
	public Transform firePoint;
	public float fireDelay;

	private GameObject newStar;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("LaunchProjectile", fireDelay, fireDelay);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void LaunchProjectile(){
		newStar = Instantiate (ninjaStar, firePoint.position, firePoint.rotation) as GameObject;
		if (transform.lossyScale.x > 0) 
		{
			newStar.GetComponent<NinjaStarController> ().rotationSpeed = -newStar.GetComponent<NinjaStarController> ().rotationSpeed;
			newStar.GetComponent<NinjaStarController> ().speed = -newStar.GetComponent<NinjaStarController> ().speed;
			newStar.GetComponent<Rigidbody2D> ().collisionDetectionMode = CollisionDetectionMode2D.Continuous;
		}
	}
}
