﻿using UnityEngine;
using System.Collections;

public class VaccumParticleController : MonoBehaviour {

	private PlayerController player;
	private ParticleSystem.EmissionModule em;

	// Use this for initialization
	void Start () {
		
		player = FindObjectOfType<PlayerController> ();
		em = GetComponent<ParticleSystem>().emission;

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonUp (1)) 
		{
			DestroyVacuumParticle ();
		}

		if(em.enabled)
			transform.position = player.vacuumPoint.position;

	}

	private IEnumerator DestroySelf()
	{
		yield return new WaitForSeconds (0.5f);
		Destroy (gameObject);
	}

	public void DestroyVacuumParticle() {
		em.enabled = false;
		StartCoroutine ("DestroySelf");
	}
}
