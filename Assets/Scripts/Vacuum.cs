﻿using UnityEngine;
using System.Collections;

public class Vacuum : MonoBehaviour {

	public GameObject vacuumParticle;

	public bool holdingProjectile;
	public float projectileSpeed;

	//private bool facingLeft;

	private Vector3 mousePos;
	private Vector3 pivotPos;
	private float pivotAngle;
	private Vector2 launchSpeed;

	// Use this for initialization
	void Start () {
		GetComponent<Collider2D> ().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponentInChildren<NinjaStarController> () == null)
			holdingProjectile = false;
		else
			holdingProjectile = true;

		if (Input.GetMouseButtonDown (1)) {
			Instantiate (vacuumParticle, transform.position, transform.rotation);
			GetComponent<Collider2D> ().enabled = true;
			/*if (FindObjectOfType<PlayerController> ().GetComponent<Transform> ().lossyScale.x < 0)
				facingLeft = true;
			else
				facingLeft = false;*/

			Debug.Log ("Current Angle: " + GetComponentInParent<VacuumPivotController> ().angle);
		}

		if (Input.GetMouseButtonUp (1)) {
			ReleaseVacuum ();
		}

		if (GetComponentInChildren<NinjaStarController>() == null)
			return;

		GetComponentInChildren<Transform> ().position = transform.position;
	}

	public void ReleaseVacuum(){
		if(FindObjectOfType<VaccumParticleController> () != null)
			FindObjectOfType<VaccumParticleController> ().DestroyVacuumParticle ();

		GetComponent<Collider2D> ().enabled = false;

		if (holdingProjectile == false)
			return;

		mousePos = transform.position;

		pivotPos = FindObjectOfType<PlayerController> ().transform.position;
		mousePos.x -= pivotPos.x;
		mousePos.y -= pivotPos.y;

		pivotAngle = Mathf.Atan2 (mousePos.y, mousePos.x);
		launchSpeed = new Vector2 (Mathf.Cos(pivotAngle) * projectileSpeed, Mathf.Sin(pivotAngle) * projectileSpeed);
		//if(facingLeft)
			//launchSpeed = -launchSpeed;
		GetComponentInChildren<Rigidbody2D> ().velocity = launchSpeed;

		holdingProjectile = false;
	}
}
