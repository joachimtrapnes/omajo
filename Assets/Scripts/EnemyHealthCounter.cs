﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyHealthCounter : MonoBehaviour {

	public static int currentEnemyHealth;

	Text text;

	private EnemyHealthManager enemy;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
		enemy = FindObjectOfType<EnemyHealthManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		text.text = "" + enemy.enemyHealth;
	}
}
