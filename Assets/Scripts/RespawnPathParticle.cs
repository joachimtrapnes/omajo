﻿using UnityEngine;
using System.Collections;

public class RespawnPathParticle : MonoBehaviour {

	private PlayerController player;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController> ();
		StartCoroutine ("DeathTimer");
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = player.transform.position;
	}

	public IEnumerator DeathTimer()
	{
		yield return new WaitForSeconds (1);
		Destroy (gameObject);
	}
}
