﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {

	public GameObject shieldParticle;

	private GameObject shield;

	// Use this for initialization
	void Start () {
		GetComponentInChildren<SpriteRenderer>().enabled = false;
		GetComponent<Collider2D> ().enabled = false;
		GetComponentInChildren<Transform> ().position = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			GetComponentInChildren<SpriteRenderer>().enabled = true;
			GetComponent<Collider2D> ().enabled = true;
		}

		if(Input.GetMouseButtonUp (0)) {
			GetComponentInChildren<SpriteRenderer>().enabled = false;
			GetComponent<Collider2D> ().enabled = false;
		}
	}
}
