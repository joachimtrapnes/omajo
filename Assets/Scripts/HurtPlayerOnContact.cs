﻿using UnityEngine;
using System.Collections;

public class HurtPlayerOnContact : MonoBehaviour {

	public int contactDamage;

	public float contactDelay;

	private PlayerController player;

	public GameObject hurtParticle;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		
		if (other.name == "Player") 
		{
			HealthManager.HurtPlayer (contactDamage);
			Instantiate (hurtParticle, player.transform.position, player.transform.rotation);
			StartCoroutine ("ContactDelay");
		}

	}

	private IEnumerator ContactDelay()
	{
		this.GetComponent<BoxCollider2D> ().enabled = false;
		yield return new WaitForSeconds (contactDelay);
		this.GetComponent<BoxCollider2D> ().enabled = true;
	}
}
