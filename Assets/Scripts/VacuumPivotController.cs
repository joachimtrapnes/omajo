﻿using UnityEngine;
using System.Collections;

public class VacuumPivotController : MonoBehaviour {

	private PlayerController player;

	private Vector3 mousePos;
	private Vector3 pivotPos;
	public float angle;

	// Use this for initialization
	void Start () {
	
		player = FindObjectOfType<PlayerController>();

	}
	
	// Update is called once per frame
	void Update () {

		transform.position = player.transform.position;

	}

	void FixedUpdate() {

		mousePos = Input.mousePosition;
		mousePos.z = 5f;

		pivotPos = Camera.main.WorldToScreenPoint (transform.position);
		mousePos.x -= pivotPos.x;
		mousePos.y -= pivotPos.y;

		angle = Mathf.Atan2 (mousePos.y, mousePos.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Euler (new Vector3 (0, 0, angle));
	}
}
